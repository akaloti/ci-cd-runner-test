/**
 * This intentionally incorrect function "aims"
 * to return the absolute value of the given argument.
 */
int myAbs(int val)
{
    return val < 0 ? -val : val;
}
