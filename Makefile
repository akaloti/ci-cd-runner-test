CC := gcc
CFLAGS := -Wall -Werror
EXECUTABLES := test_abs
OBJ_FILES := abs.o

all: $(EXECUTABLES)

test_abs: test_abs.c abs.o
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(EXECUTABLES) $(OBJ_FILES)
