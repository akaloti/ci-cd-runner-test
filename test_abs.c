#include <assert.h>
#include <stdio.h>

extern int myAbs(int val);

int main()
{
    assert(myAbs(3) == 3);
    assert(myAbs(-5) == 5);
}
